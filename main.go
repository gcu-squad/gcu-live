package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"regexp"
	"time"

	"github.com/spf13/viper"
	"golang.org/x/net/html/charset"
	"golang.org/x/text/transform"
)

type LogLine struct {
	Time     string `json:"time"`
	Nickname string `json:"nickname"`
	Phrase   string `json:"phrase"`
	Action   bool   `json:"action"`
	URL      string `json:"url,omitempty"`
	Last     bool   `json:"last,omitempty"`
}

func cleanText(text string) (string, error) {
	data := []byte(text)

	_, name, _ := charset.DetermineEncoding(data, "")

	if name == "utf-8" || name == "us-ascii" {
		return text, nil
	}

	e, _ := charset.Lookup(name)
	if e == nil {
		return "", fmt.Errorf("unsupported encoding: %s", name)
	}

	// Create a transformer to convert from the detected encoding to UTF-8
	reader := transform.NewReader(bytes.NewReader(data), e.NewDecoder())

	// Read from the transformer to get the converted data
	convertedData, err := ioutil.ReadAll(reader)
	if err != nil {
		return "", err
	}

	return string(convertedData), nil
}

func main() {
	var arcdate string

	flag.StringVar(&arcdate, "date", "", "specify the log date in format ddmmyyyy")
	flag.Parse()

	// Load configuration
	viper.SetConfigName("irclog2json") // name of config file (without extension)
	viper.SetConfigType("ini")         // REQUIRED if the config file does not have the extension in the name
	viper.AddConfigPath("$HOME/etc")   // path to look for the config file
	err := viper.ReadInConfig()        // Find and read the config file
	if err != nil {
		fmt.Printf("Fatal error config file: %s \n", err)
		os.Exit(1)
	}

	ircpath := viper.GetString("pinpin.ircpath")
	wwwpath := viper.GetString("pinpin.wwwpath")
	jsonfile := viper.GetString("pinpin.jsonfile")

	now := time.Now()
	if arcdate == "" {
		ircpath = fmt.Sprintf("%s.%s", ircpath, now.Format("02Jan2006"))
		wwwpath = fmt.Sprintf("%s/%s.json", wwwpath, jsonfile)
	} else {
		ircpath = fmt.Sprintf("%s.%s", ircpath, arcdate)
		wwwpath = fmt.Sprintf("%s/history/%s.%s.json", wwwpath, jsonfile, arcdate)
	}

	// Regular expressions
	msgRegex := regexp.MustCompile(`\[(.+?)\] <(.+?)> (.+)`)
	actionRegex := regexp.MustCompile(`\[(.+?)\] Action: ([^ ]+) (.+)`)
	urlRegex := regexp.MustCompile(`.*((https?://)+[^\ \n\x0f$]+)`)

	logLines := []LogLine{}
	file, err := os.Open(ircpath)
	if err != nil {
		fmt.Printf("Error opening file: %s\n", err)
		return
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line, err := cleanText(scanner.Text())
		if err != nil {
			fmt.Printf("Encoding error: %s\n", err)
			continue
		}

		var logLine LogLine

		// Skip lines containing "nolog"
		if regexp.MustCompile(`[\[#]\ ?nolog\ ?[#\]]?`).MatchString(line) {
			continue
		}

		if msgMatches := msgRegex.FindStringSubmatch(line); msgMatches != nil {
			logLine.Time = msgMatches[1]
			logLine.Nickname = msgMatches[2]
			logLine.Phrase = msgMatches[3]
			logLine.Action = false
		} else if actionMatches := actionRegex.FindStringSubmatch(line); actionMatches != nil {
			logLine.Time = actionMatches[1]
			logLine.Nickname = actionMatches[2]
			logLine.Phrase = actionMatches[3]
			logLine.Action = true
		} else {
			continue
		}

		if urlMatches := urlRegex.FindStringSubmatch(logLine.Phrase); urlMatches != nil {
			logLine.URL = urlMatches[1]
		}

		logLines = append(logLines, logLine)
	}

	if len(logLines) > 0 {
		logLines[len(logLines)-1].Last = true
	}

	jsonData, err := json.MarshalIndent(logLines, "", "    ")
	if err != nil {
		fmt.Printf("Error marshalling JSON: %s\n", err)
		return
	}

	err = ioutil.WriteFile(wwwpath, jsonData, 0644)
	if err != nil {
		fmt.Printf("Error writing JSON file: %s\n", err)
		return
	}
}
