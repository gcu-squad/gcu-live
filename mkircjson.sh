#!/bin/sh

ircpath=$(egrep -o '/.+gcu\.log' ${HOME}/etc/irclog2json.ini)
wwwpath=$(egrep -o '/.+www.+$' ${HOME}/etc/irclog2json.ini)
jsonpath="${wwwpath}/history"
histtmp="${wwwpath}/history.presort"
histlist="${wwwpath}/history.lst"

[ ! -d "$jsonpath" ] && mkdir -p ${jsonpath}

echo -e '[\n{"date": "now","logfile":"now"},' >  ${histtmp}
for l in ${ircpath}*
do
    d=${l#*log.}
    n=$(date +%d%b%Y)
    [ "${d}" = "${n}" ] && continue
    log="gcu_irc_log.${d}.json"
    f="${jsonpath}/${log}"
    [ ! -f "$f" ] && ${HOME}/bin/irclog2json.py ${d}
    o=$(date -d "$d" "+%Y/%m/%d")
    echo "{\"date\":\"${o}\",\"logfile\":\"${d}\"}," >>${histtmp}
done
sed -i '$ s/,$//' ${histtmp}
echo ']' >>  ${histtmp}

cat ${histtmp}|jq '.|sort_by(.date)' >${histlist}
rm -f ${histtmp}
